Hey, thanks for using this circuit simulator! 

It was designed and programmed by Daniel Koenig in Java using NetBeans 11 for CS-353. 

You can find more of my work at https://gitlab.com/Rvby1.

###HOW TO USE###
This simulator is pretty simple to use. You basically want to build the circuit using a properly-formatted text file. 

The text file should look something like this: 
/This is a comment! 
AND A1 S1 S2 	/this establishes an AND gate called A1 with inputs S1 and S2
OR A2 S1 S3 	/this establishes an OR gate called A2 with inputs S1 and S3
XOR A3 A1 A2 	/this establishes an XOR gate called A3 with inputs A1 and A2 (which we previously created!)
NOT A4 A3 		/this establishes a NOT gate called A4 with A3 as its input
LIGHT L1 A3 	/this establishes a connection between light 1 (L1) and the output of A3

As you can see, you define most gates with this syntax: GATETYPE GATENAME INPUT1 INPUT2. These definitions all need to be on their own line to be counted. 

You can make comments by preceeding what you're saying with a /, but remember that EVERYTHING after the / on the same line will be considered a comment.

The lights and light switches have already been defined for you. The lights have the names S1-S6, while the lightbulbs have the names L1-L4. 

You can establish gates of these types: AND, OR, NOR, XOR, NXOR, NAND, NOT, and LIGHT. 

Please note that you must establish a gate before you use it as input for another gate. In other words, this is invalid: 
AND A2 A1 S1	/This will throw an error because the gate A1 hasn't been defined yet!
OR A1 S1 S3 	

###DIAGNOSTIC TOOLS###
To make it easier for you to see the current state of your circuit, you can enter the name of the gate in the text box next to "Show Gate Tree". 

If you enter a valid gate name, a new window will pop up with the state information for the gate you entered and all the children gates connected to the gate you entered!


Thanks! Enjoy using the simulator! :) 