package rvby1.lab4.circuitmaker;

import rvby1.lab4.circuitmaker.Gates.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A gate that will perform an AND logical operation on its input gates
 * @author Daniel Koenig
 */
public class CircuitBuilder {
    private BufferedReader bufferedReader;
    
    public CircuitBuilder() { 
        
    }
    /**
     * Constructs a circuit from a properly formatted text file. 
     * @param circuitTextFile text file where the circuit information is stored 
     * @param gatesHashMap a hash map that will store all of the created circuits; contains the lights and switches when it is passed in
     * @throws CircuitFormatError thrown if there is an issue with the circuit file and contains a message detailing the error
     */
    public void buildCircuitFromTextFile(File circuitTextFile, HashMap<String, BaseGate> gatesHashMap) throws CircuitFormatError { 
       
        //open up the buffered reader, then read each line from the text file and parse it
        String currentLineString;
        int currentLineNumber = 0;
        try {
            bufferedReader = new BufferedReader(new FileReader(circuitTextFile));
            
            while((currentLineString = bufferedReader.readLine()) != null) {
                //index values of the different sections: 
                int gateTypeIndex = 0;
                int gateNameIndex = 1;
                int inputGateOneIndex = 2;
                int inputGateTwoIndex = 3;
                
                //holders for gate information:
                String gateType = "";
                String gateName = "";
                BaseGate inputGateOne = null;
                BaseGate inputGateTwo = null;
                
                //if we're making a light, this lets us figure out critical info about the light: 
                BaseGate lightToModify = null;
                
                //check if the line is a comment by seeing if the first character is a /
                if(currentLineString.charAt(0) == '/') { 
                    currentLineNumber += 1;
                    continue;
                }
                
                //split the line into its individual parts by the spaces:
                String [] splitCurrentLine = currentLineString.split("\\s+");
                
                //get the proper data from the strings: 
                gateType = splitCurrentLine[gateTypeIndex].toUpperCase();

                
                switch(gateType) { 
                    case "AND":
                    case "OR":
                    case "NOR":
                    case "XOR":
                    case "XNOR":
                    case "NAND":
                        //get the gate name and check whether it's already being used: 
                        gateName = splitCurrentLine[gateNameIndex].toUpperCase();
                        if(gatesHashMap.containsKey(gateName)) { 
                            throw new CircuitFormatError("Gate name '" + gateName + "' on line " + currentLineNumber + " already in use!");
                        }
                        
                        //get the input gates and check if the gate names were valid: 
                        inputGateOne = gatesHashMap.get(splitCurrentLine[inputGateOneIndex]);
                        if(inputGateOne == null) { 
                            throw new CircuitFormatError("Gate name '" + splitCurrentLine[inputGateOneIndex] + "' on line " + currentLineNumber + " doesn't exist!");
                        }
                        
                        inputGateTwo = gatesHashMap.get(splitCurrentLine[inputGateTwoIndex]);
                        if(inputGateTwo == null) { 
                            throw new CircuitFormatError("Gate name '" + splitCurrentLine[inputGateOneIndex] + "' on line " + currentLineNumber + " doesn't exist!");
                        }
                        break;
                    case "NOT": 
                        gateName = splitCurrentLine[gateNameIndex].toUpperCase();
                        inputGateOne = gatesHashMap.get(splitCurrentLine[inputGateOneIndex]);
                        break;
                    case "LIGHT":
                        //check to see if the gate exists and whether or not the it's a light
                        gateName = splitCurrentLine[gateNameIndex].toUpperCase();
                        lightToModify = gatesHashMap.get(gateName);
                        if(lightToModify == null) { 
                            throw new CircuitFormatError("Light '" + gateName + "' on line " + currentLineNumber + " doesn't exist!");
                        } 
                        if(!lightToModify.getGateType().equals("LIGHT")) { 
                            throw new CircuitFormatError("Gate '" + gateName + "' on line " + currentLineNumber + " isn't a light!");
                        }
                        inputGateOne = gatesHashMap.get(splitCurrentLine[inputGateOneIndex]);
                        break;
                    default: 
                        System.out.println("Error! Incorrect gate type syntax on line " + currentLineNumber);
                        throw new CircuitFormatError("Incorrect gate type syntax on line "  + currentLineNumber);
                }
                
                //create the circuit using the collected information:
                switch(gateType) { 
                    case "AND": 
                        gatesHashMap.put(gateName, new AND_Gate(inputGateOne, inputGateTwo, gateName));
                        break;
                    case "OR": 
                        gatesHashMap.put(gateName, new OR_Gate(inputGateOne, inputGateTwo, gateName));
                        break;
                    case "XOR": 
                        gatesHashMap.put(gateName, new XOR_Gate(inputGateOne, inputGateTwo, gateName));
                        break;
                    case "NOR":
                        gatesHashMap.put(gateName, new NOR_Gate(inputGateOne, inputGateTwo, gateName));
                        break;
                    case "XNOR": 
                        gatesHashMap.put(gateName, new XNOR_Gate(inputGateOne, inputGateTwo, gateName));
                        break;
                    case "NAND": 
                        gatesHashMap.put(gateName, new NAND_Gate(inputGateOne, inputGateTwo, gateName));
                        break;
                    case "NOT":
                        gatesHashMap.put(gateName, new NOT_Gate(inputGateOne, inputGateTwo, gateName));
                        break;
                    case "LIGHT": 
                        ((Light)lightToModify).setInputGate(inputGateOne);
                        break;
                }
                currentLineNumber += 1;
            }
        } catch (IOException ex) {
            throw new CircuitFormatError("Error reading text file! Are you sure it exists?");
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException ex) {
                throw new CircuitFormatError("Error closing text file!");
            }
        }
        
    }
}
