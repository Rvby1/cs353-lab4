/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvby1.lab4.circuitmaker;

/**
 *
 * @author Daniel
 */
public class CircuitFormatError extends Exception {

    /**
     * Creates a new instance of <code>CircuitFormatError</code> without detail
     * message.
     */
    public CircuitFormatError() {
    }

    /**
     * Constructs an instance of <code>CircuitFormatError</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public CircuitFormatError(String msg) {
        super(msg);
    }
}
