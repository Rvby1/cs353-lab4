package rvby1.lab4.circuitmaker.Gates;

/**
 * A gate that will perform an NOT logical operation on its input gate
 * @author Daniel Koenig
 */
public class NOT_Gate extends BaseGate {
    public NOT_Gate(BaseGate inputGateOne, BaseGate inputGateTwo, String circuitName) {
        super(inputGateOne, inputGateTwo, circuitName);
        gateType = "NOT";
    }

    /**
     * Determines current value of this gate using NOT
     */
    @Override
    public void determineCircuitValue() {
        currentValue = !inputGateOne.currentValue;
        printCircuitValue();
    } 
}
