package rvby1.lab4.circuitmaker.Gates;

import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
//
/**
 * A gate that functions like a light switch and provides an ON or OFF signal depending on the state of its GUI element
 * @author Daniel Koenig
 */
public abstract class ToggleableGate extends BaseGate {
    protected JLabel toggleGate_label;
    
    protected final String toggleGateOnString = "ON";
    protected final String toggleGateOffString = "OFF";
    
    protected ImageIcon toggleGateOnImage; 
    protected ImageIcon toggleGateOffImage; 

    public ToggleableGate(BaseGate inputGateOne, BaseGate inputGateTwo, String gateName, JLabel toggleGate_label, 
            ImageIcon toggleGateOnImage, ImageIcon toggleGateOffImage) {
        super(inputGateOne, inputGateTwo, gateName);
        
        this.toggleGateOnImage = toggleGateOnImage;
        this.toggleGateOffImage = toggleGateOffImage;
        
        this.toggleGate_label = toggleGate_label;
        
        currentValue = false;
        this.toggleGate_label.setIcon(toggleGateOffImage);
    }
   
    public abstract void determineCircuitValue();
}
