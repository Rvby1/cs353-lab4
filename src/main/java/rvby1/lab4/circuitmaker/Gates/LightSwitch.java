package rvby1.lab4.circuitmaker.Gates;

import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
//
/**
 * A gate that functions like a light switch and provides an ON or OFF signal depending on the state of its GUI element
 * @author Daniel Koenig
 */
public class LightSwitch extends ToggleableGate {
    
    public LightSwitch(BaseGate inputGateOne, BaseGate inputGateTwo, String gateName, JLabel lightSwitch_label, 
            ImageIcon lightSwitchOnImage, ImageIcon lightSwitchOffImage) {
        super(inputGateOne, inputGateTwo, gateName, lightSwitch_label, lightSwitchOnImage, lightSwitchOffImage);
        
        gateType = "SWITCH";
    }
    
    /**
     * Toggles the current value of the switch and updates the appearance of the GUI element
     */
    public void toggleSwitch() { 
        currentValue = !currentValue;
        if(currentValue) { 
            toggleGate_label.setIcon(toggleGateOnImage);
        } else { 
            toggleGate_label.setIcon(toggleGateOffImage);
        }

        //the value of the switch has updated, so update outputs 
        updateOutputCircuits();
    }
   
    @Override
    public void determineCircuitValue() {
        //do nothing
    }
    
    /**
     * Called when the light's value changes
     */
    @Override
    public void updateOutputCircuits() {        
        for(BaseGate currentOutputCircuit : gatesToOutputTo ) {
            currentOutputCircuit.updateOutputCircuits();
        }
    }
}
