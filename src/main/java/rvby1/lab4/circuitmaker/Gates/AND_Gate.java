package rvby1.lab4.circuitmaker.Gates;

/**
 * A gate that will perform an AND logical operation on its input gates
 * @author Daniel Koenig
 */
public class AND_Gate extends BaseGate {
    public AND_Gate(BaseGate inputGateOne, BaseGate inputGateTwo, String circuitName) {
        super(inputGateOne, inputGateTwo, circuitName);
        gateType = "AND";
    }

    /**
     * Determines current value of this gate using AND
     */
    @Override
    public void determineCircuitValue() {
        if(inputGateOne.getCurrentValue() && inputGateTwo.getCurrentValue()) { 
            currentValue = true;
        } else { 
            currentValue = false;
        }
        
        printCircuitValue();
    } 
}
