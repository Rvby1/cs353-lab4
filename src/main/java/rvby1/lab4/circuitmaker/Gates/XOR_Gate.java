package rvby1.lab4.circuitmaker.Gates;

/**
 * A gate that will perform an XOR logical operation on its input gates
 * @author Daniel Koenig
 */
public class XOR_Gate extends BaseGate {
    
    public XOR_Gate(BaseGate inputGateOne, BaseGate inputGateTwo, String circuitName) {
        super(inputGateOne, inputGateTwo, circuitName);
        gateType = "XOR";
    }

    /**
     * Determines current value of this gate using XOR
     */
    @Override
    public void determineCircuitValue() {
        if((inputGateOne.getCurrentValue() || inputGateTwo.getCurrentValue()) && 
                !(inputGateOne.getCurrentValue() && inputGateTwo.getCurrentValue())) { 
            currentValue = true;
        } else { 
            currentValue = false;
        }
        
        printCircuitValue();
    } 
}
