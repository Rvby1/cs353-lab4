package rvby1.lab4.circuitmaker.Gates;

/**
 * A gate that will perform an XNOR logical operation on its input gates
 * @author Daniel Koenig
 */
public class XNOR_Gate extends BaseGate {
    
    public XNOR_Gate(BaseGate inputGateOne, BaseGate inputGateTwo, String circuitName) {
        super(inputGateOne, inputGateTwo, circuitName);
        gateType = "XNOR";
    }

    /**
     * Determines current value of this gate using XNOR
     */
    @Override
    public void determineCircuitValue() {
        if((inputGateOne.getCurrentValue() && inputGateTwo.getCurrentValue()) 
                || !(inputGateOne.getCurrentValue() || inputGateTwo.getCurrentValue())) { 
            currentValue = true;
        } else { 
            currentValue = false;
        }
        
        printCircuitValue();
    } 
}
