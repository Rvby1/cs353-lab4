package rvby1.lab4.circuitmaker.Gates;

/**
 * A gate that will perform a NOR logical operation on its input gates
 * @author Daniel Koenig
 */
public class NOR_Gate extends BaseGate {
    public NOR_Gate(BaseGate inputGateOne, BaseGate inputGateTwo, String circuitName) {
        super(inputGateOne, inputGateTwo, circuitName);
        gateType = "NOR";
    }

    /**
     * Determines current value of this gate using NOR
     */
    @Override
    public void determineCircuitValue() {
        if(!(inputGateOne.getCurrentValue() || inputGateTwo.getCurrentValue())) { 
            currentValue = true;
        } else { 
            currentValue = false;
        }
        
        printCircuitValue();
    } 
}
