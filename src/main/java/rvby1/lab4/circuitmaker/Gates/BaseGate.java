package rvby1.lab4.circuitmaker.Gates;


import java.util.ArrayList;

/**
 * A base class that contains all the basic information and methods for all kinds of logical gates
 * @author Daniel Koenig
 */
public abstract class BaseGate {
    protected BaseGate inputGateOne;
    protected BaseGate inputGateTwo;
    
    protected boolean currentValue;
    
    protected ArrayList<BaseGate> gatesToOutputTo;
    protected String gateName;
    protected String gateType;
    
    /**
     * Constructor. 
     * @param inputGateOne the first input for the gate
     * @param inputGateTwo the second input for the gate. This will sometimes be null
     * @param circuitName the name of the circuit so that it can be referenced from a list of circuits
     */
    public BaseGate(BaseGate inputGateOne, BaseGate inputGateTwo, String circuitName) { 
        this.gateName = circuitName;
        this.inputGateOne = inputGateOne;
        this.inputGateTwo = inputGateTwo;
        if(inputGateOne != null) { 
            inputGateOne.addOuputCircuit(this);
        }
        if(inputGateTwo != null) { 
            inputGateTwo.addOuputCircuit(this);
        }
        
        determineCircuitValue();
        gatesToOutputTo = new ArrayList<BaseGate>();
        
        gateType = "ERROR";
    }
    
    public String getGateType() { 
        return gateType;
    }
    
    public boolean getCurrentValue () { 
        return currentValue;
    }
    
    public String getGateName() { 
        return gateName;
    }
    
    /**
     * Adds outputCircuitToAdd to the list of output circuits
     * @param outputCircuitToAdd output circuit to be added to the list of output circuits 
     */
    public void addOuputCircuit(BaseGate outputCircuitToAdd) { 
        gatesToOutputTo.add(outputCircuitToAdd);
    }
    
    /**
     * Determines what the circuit's current value should be 
     */
    public abstract void determineCircuitValue();
    
    /**
     * Called when an input circuit's value changes. If this causes this circuit's value to change, update all output circuits. 
     */
    public void updateOutputCircuits() { 
        //check if the circuit value changes after being rechecked
        boolean circuitValueBeforeRechecking = currentValue;
        determineCircuitValue();
        
        //if the value changed, then we need to let the circuits connected to this circuit know 
        if(circuitValueBeforeRechecking != currentValue) { 
            for(BaseGate currentOutputCircuit : gatesToOutputTo ) {
                currentOutputCircuit.updateOutputCircuits();
            }
        }
    }
    
    /**
     * Removes all references to gates receiving output from the calling gate, including children of those gates; effectively marks these gates for garbage collection
     */
    public void removeOutputGates() { 
        for(BaseGate gateToRemove : gatesToOutputTo) { 
            gateToRemove.removeOutputGates();
        }
        
        //remove references to all output gates now that we have had them all get cleared
        gatesToOutputTo.clear();
        
        //remove references to input gates now to "remove" this gate from the system 
        inputGateOne = null;
        inputGateTwo = null;
    }
    
    /**
     * Creates a string with the output gates connected to this gate, including any outputs of those gates, in a tree-like fashion
     * @param numberOfTabs the number of tabs to place before the gate names
     * @return the string of the current gate with the information for all of its output gates
     */
    public String createOutputConnectionsTree(int numberOfTabs) {
        boolean inputGateOneValue;
        boolean inputGateTwoValue;
        String stringToPrint;
        
        String tabsToAppend = "";
        for(int i = 0; i < numberOfTabs; i++) { 
            tabsToAppend += "\t";
        }
        
        stringToPrint = tabsToAppend + gateType + " gate " + gateName + "'s current value is " + currentValue + ".";
                
        if(inputGateOne != null) { 
            stringToPrint += " Input 1 is " + inputGateOne.getCurrentValue() + ".";
        }
        
        if(inputGateTwo != null) { 
            stringToPrint += " Input 2 is " + inputGateTwo.getCurrentValue() + ".";
        } 
               
        System.out.println(stringToPrint);
        stringToPrint += System.lineSeparator();
        
        for(BaseGate circuitToPrint : gatesToOutputTo) { 
            stringToPrint += circuitToPrint.createOutputConnectionsTree(numberOfTabs + 1);
        }
        
        return stringToPrint;
    }
    
    /**
     * Prints the current value of the circuit
     */
    protected void printCircuitValue() {
        System.out.println("Current Value of " + gateType + " gate " + gateName + ": " + currentValue);
    }
}
