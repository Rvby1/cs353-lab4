package rvby1.lab4.circuitmaker.Gates;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * A gate that will perform an AND logical operation on its input gates
 * @author Daniel Koenig
 */
public class Light extends ToggleableGate {

    public Light(BaseGate inputGateOne, BaseGate inputGateTwo, String circuitName, JLabel light_label, 
            ImageIcon lightOnImage, ImageIcon lightOffImage) {
        super(inputGateOne, inputGateTwo, circuitName, light_label, lightOnImage, lightOffImage);
        
        gateType = "LIGHT";
    }
    
    /**
     * Sets inputCircuitOne to the new input circuit
     * @param newInputCircuit the new gate to be the first input circuit
     */
    public void setInputGate(BaseGate newInputCircuit) { 
        inputGateOne = newInputCircuit;
        inputGateOne.addOuputCircuit(this);
    }

    /**
     * Determines current value of this gate using AND
     */
    @Override
    public void determineCircuitValue() {
        if(inputGateOne != null) { 
            currentValue = inputGateOne.getCurrentValue();
            if(currentValue) { 
                toggleGate_label.setIcon(toggleGateOnImage);
            } else { 
                toggleGate_label.setIcon(toggleGateOffImage);
            }
        }

        printCircuitValue();
    } 
}
