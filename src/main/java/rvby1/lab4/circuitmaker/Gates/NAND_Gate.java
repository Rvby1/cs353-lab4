package rvby1.lab4.circuitmaker.Gates;

/**
 * A gate that will perform an NAND logical operation on its input gates
 * @author Daniel Koenig
 */
public class NAND_Gate extends BaseGate {
    public NAND_Gate(BaseGate inputGateOne, BaseGate inputGateTwo, String circuitName) {
        super(inputGateOne, inputGateTwo, circuitName);
        gateType = "NAND";
    }

    /**
     * Determines current value of this gate using NAND
     */
    @Override
    public void determineCircuitValue() {
        if(!(inputGateOne.getCurrentValue() && inputGateTwo.getCurrentValue())) { 
            currentValue = true;
        } else { 
            currentValue = false;
        }
        
        printCircuitValue();
    } 
}
