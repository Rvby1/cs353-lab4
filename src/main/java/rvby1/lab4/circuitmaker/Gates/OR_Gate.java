package rvby1.lab4.circuitmaker.Gates;

/**
 * A gate that will perform an OR logical operation on its input gates
 * @author Daniel Koenig
 */
public class OR_Gate extends BaseGate {
    public OR_Gate(BaseGate inputGateOne, BaseGate inputGateTwo, String circuitName) {
        super(inputGateOne, inputGateTwo, circuitName);
        gateType = "OR";
    }

    /**
     * Determines current value of this gate using OR
     */
    @Override
    public void determineCircuitValue() {
        if(inputGateOne.getCurrentValue() || inputGateTwo.getCurrentValue()) { 
            currentValue = true;
        } else { 
            currentValue = false;
        }
        
        printCircuitValue();
    } 
}
