package rvby1.lab4.circuitmaker;

import rvby1.lab4.circuitmaker.Gates.*;
import java.awt.Color;
import java.awt.Point;
import java.awt.TextField;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.Console;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import sun.rmi.runtime.Log;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Daniel
 */
public class MainFrame extends javax.swing.JFrame {
    CircuitBuilder circuitBuilder;
    
    LightSwitch[] lightSwitches;
    
    Light[] lights;

    HashMap<String, BaseGate> gatesHashMap; 
    
    ImageIcon lightSwitchOnImage;
    ImageIcon lightSwitchOffImage; 
    ImageIcon lightOnImage;
    ImageIcon lightOffImage;
    
    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initComponents();
        
        //try to load all of the images for the switches and lights. If it fails, just exit. 
        try {
            lightSwitchOnImage = createIconImageFromPath("/switch_on.png");
            lightSwitchOffImage = createIconImageFromPath("/switch_off.png");
            lightOnImage = createIconImageFromPath("/light_on.png");
            lightOffImage = createIconImageFromPath("/light_off.png");
        
        } catch (IOException ex) {
            showErrorMessage("Error Loading Toggle Images", "Could not load one of the toggle images!");
            return;
        }

        //create light switches with null input circuits since they don't matter for the switch
        lightSwitches = new LightSwitch[6];
        lightSwitches[0] = new LightSwitch(null, null, "S1", lightSwitch1_lbl, lightSwitchOnImage, lightSwitchOffImage);
        lightSwitches[1] = new LightSwitch(null, null, "S2", lightSwitch2_lbl, lightSwitchOnImage, lightSwitchOffImage);
        lightSwitches[2] = new LightSwitch(null, null, "S3", lightSwitch3_lbl, lightSwitchOnImage, lightSwitchOffImage);
        lightSwitches[3] = new LightSwitch(null, null, "S4", lightSwitch4_lbl, lightSwitchOnImage, lightSwitchOffImage); 
        lightSwitches[4] = new LightSwitch(null, null, "S5", lightSwitch5_lbl, lightSwitchOnImage, lightSwitchOffImage);
        lightSwitches[5] = new LightSwitch(null, null, "S6", lightSwitch6_lbl, lightSwitchOnImage, lightSwitchOffImage); 
        
        //create lights with null input circuits since they don't yet have any inputs:
        lights = new Light[4];
        lights[0] = new Light(null, null, "L1", light1_lbl, lightOnImage, lightOffImage);
        lights[1] = new Light(null, null, "L2", light2_lbl, lightOnImage, lightOffImage);
        lights[2] = new Light(null, null, "L3", light3_lbl, lightOnImage, lightOffImage);
        lights[3] = new Light(null, null, "L4", light4_lbl, lightOnImage, lightOffImage);
        
        gatesHashMap = new HashMap<>();

        circuitBuilder = new CircuitBuilder();
    }

    /**
     * Creates an image icon using the image located at the path
     * @param imagePath the path relative to the root of the target build of the image
     * @return the image icon that was created using the image located at the path
     * @throws IOException thrown when the 
     */
    public ImageIcon createIconImageFromPath(String imagePath) throws IOException { 
        return new ImageIcon(ImageIO.read(this.getClass().getResource(imagePath)));
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        lightSwitch_panel = new javax.swing.JPanel();
        lightSwitch1_lbl = new javax.swing.JLabel();
        lightSwitch2_lbl = new javax.swing.JLabel();
        lightSwitch3_lbl = new javax.swing.JLabel();
        lightSwitch4_lbl = new javax.swing.JLabel();
        light_panel = new javax.swing.JPanel();
        light1_lbl = new javax.swing.JLabel();
        light2_lbl = new javax.swing.JLabel();
        light3_lbl = new javax.swing.JLabel();
        light4_lbl = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        openCircuitTextFile_btn = new javax.swing.JButton();
        displayGateInfo_text = new javax.swing.JTextField();
        showGateTree_btn = new javax.swing.JButton();
        showProgramInfo_btn = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        lightSwitch_panel1 = new javax.swing.JPanel();
        lightSwitch5_lbl = new javax.swing.JLabel();
        lightSwitch6_lbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Lab 4 - Circuit Simulator");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setMinimumSize(new java.awt.Dimension(710, 541));
        setSize(new java.awt.Dimension(710, 541));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 8)); // NOI18N
        jLabel1.setText("Lightbulb Icon by Molly Bramlet from Noun Project");

        lightSwitch_panel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        lightSwitch1_lbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/switch_off.png"))); // NOI18N
        lightSwitch1_lbl.setText("Switch 1");
        lightSwitch1_lbl.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lightSwitch1_lbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lightSwitch1_lblMouseClicked(evt);
            }
        });

        lightSwitch2_lbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/switch_off.png"))); // NOI18N
        lightSwitch2_lbl.setText("Switch 2");
        lightSwitch2_lbl.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lightSwitch2_lbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lightSwitch2_lblMouseClicked(evt);
            }
        });

        lightSwitch3_lbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/switch_off.png"))); // NOI18N
        lightSwitch3_lbl.setText("Switch 3");
        lightSwitch3_lbl.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lightSwitch3_lbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lightSwitch3_lblMouseClicked(evt);
            }
        });

        lightSwitch4_lbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/switch_off.png"))); // NOI18N
        lightSwitch4_lbl.setText("Switch 4");
        lightSwitch4_lbl.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lightSwitch4_lbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lightSwitch4_lblMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout lightSwitch_panelLayout = new javax.swing.GroupLayout(lightSwitch_panel);
        lightSwitch_panel.setLayout(lightSwitch_panelLayout);
        lightSwitch_panelLayout.setHorizontalGroup(
            lightSwitch_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lightSwitch_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(lightSwitch_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lightSwitch4_lbl)
                    .addComponent(lightSwitch3_lbl)
                    .addComponent(lightSwitch2_lbl)
                    .addComponent(lightSwitch1_lbl))
                .addGap(30, 30, 30))
        );
        lightSwitch_panelLayout.setVerticalGroup(
            lightSwitch_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lightSwitch_panelLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(lightSwitch1_lbl)
                .addGap(50, 50, 50)
                .addComponent(lightSwitch2_lbl)
                .addGap(50, 50, 50)
                .addComponent(lightSwitch3_lbl)
                .addGap(50, 50, 50)
                .addComponent(lightSwitch4_lbl)
                .addGap(30, 30, 30))
        );

        light_panel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        light1_lbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/light_off.png"))); // NOI18N
        light1_lbl.setText("Light 1");

        light2_lbl.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        light2_lbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/light_off.png"))); // NOI18N
        light2_lbl.setText("Light 2");

        light3_lbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/light_off.png"))); // NOI18N
        light3_lbl.setText("Light 3");

        light4_lbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/light_off.png"))); // NOI18N
        light4_lbl.setText("Light 4");

        javax.swing.GroupLayout light_panelLayout = new javax.swing.GroupLayout(light_panel);
        light_panel.setLayout(light_panelLayout);
        light_panelLayout.setHorizontalGroup(
            light_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(light_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(light_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(light1_lbl)
                    .addComponent(light4_lbl)
                    .addComponent(light2_lbl)
                    .addComponent(light3_lbl))
                .addGap(30, 30, 30))
        );
        light_panelLayout.setVerticalGroup(
            light_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(light_panelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(light1_lbl, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(light2_lbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(light3_lbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(light4_lbl)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        openCircuitTextFile_btn.setText("Open Circuit File (.txt)");
        openCircuitTextFile_btn.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        openCircuitTextFile_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openCircuitTextFile_btnActionPerformed(evt);
            }
        });

        displayGateInfo_text.setText("S1");
        displayGateInfo_text.setToolTipText("Enter a gate name");

        showGateTree_btn.setText("Show Gate Tree");
        showGateTree_btn.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        showGateTree_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showGateTree_btnActionPerformed(evt);
            }
        });

        showProgramInfo_btn.setText("Show Program Info");
        showProgramInfo_btn.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        showProgramInfo_btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showProgramInfo_btnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(displayGateInfo_text)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(showGateTree_btn))
                    .addComponent(openCircuitTextFile_btn, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                    .addComponent(showProgramInfo_btn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(openCircuitTextFile_btn)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(displayGateInfo_text)
                    .addComponent(showGateTree_btn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(showProgramInfo_btn)
                .addContainerGap())
        );

        jLabel3.setFont(new java.awt.Font("Open Sans", 0, 36)); // NOI18N
        jLabel3.setText("Circuit Simulator");

        lightSwitch_panel1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        lightSwitch5_lbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/switch_off.png"))); // NOI18N
        lightSwitch5_lbl.setText("Switch 5");
        lightSwitch5_lbl.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lightSwitch5_lbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lightSwitch5_lblMouseClicked(evt);
            }
        });

        lightSwitch6_lbl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/switch_off.png"))); // NOI18N
        lightSwitch6_lbl.setText("Switch 6");
        lightSwitch6_lbl.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lightSwitch6_lbl.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lightSwitch6_lblMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout lightSwitch_panel1Layout = new javax.swing.GroupLayout(lightSwitch_panel1);
        lightSwitch_panel1.setLayout(lightSwitch_panel1Layout);
        lightSwitch_panel1Layout.setHorizontalGroup(
            lightSwitch_panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lightSwitch_panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lightSwitch5_lbl)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lightSwitch6_lbl)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        lightSwitch_panel1Layout.setVerticalGroup(
            lightSwitch_panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lightSwitch_panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(lightSwitch_panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lightSwitch5_lbl)
                    .addComponent(lightSwitch6_lbl))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lightSwitch_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(lightSwitch_panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(light_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(light_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lightSwitch_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel3)
                        .addGap(171, 171, 171)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lightSwitch_panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void openCircuitTextFile_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openCircuitTextFile_btnActionPerformed
        final JFileChooser fileChooser = new JFileChooser();
        int fileChooserReturn = fileChooser.showOpenDialog(this);
        
        //if the user chooses a file: 
        if(fileChooserReturn == JFileChooser.APPROVE_OPTION) { 
            //clear data from any previous circuit: 
            gatesHashMap.clear();
            System.out.println(lightSwitches.length);
            for(int i = 0; i < lightSwitches.length; i++) { 
                lightSwitches[i].removeOutputGates();
            }
            
            //add the static lights and switches:
            for(ToggleableGate currentToggleGate : lightSwitches) { 
                gatesHashMap.put(currentToggleGate.getGateName(), currentToggleGate);
            }
            
            for(ToggleableGate currentToggleGate : lights) { 
                gatesHashMap.put(currentToggleGate.getGateName(), currentToggleGate);
            }
            
            File file = fileChooser.getSelectedFile();

            try {
                circuitBuilder.buildCircuitFromTextFile(file, gatesHashMap);
            } catch (CircuitFormatError ex) {
                showErrorMessage("Circuit Builder Error!", ex.getMessage());
            }
        }
    }//GEN-LAST:event_openCircuitTextFile_btnActionPerformed

    private void lightSwitch1_lblMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lightSwitch1_lblMouseClicked
        lightSwitches[0].toggleSwitch();
    }//GEN-LAST:event_lightSwitch1_lblMouseClicked

    private void lightSwitch3_lblMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lightSwitch3_lblMouseClicked
        lightSwitches[2].toggleSwitch();
    }//GEN-LAST:event_lightSwitch3_lblMouseClicked

    private void lightSwitch4_lblMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lightSwitch4_lblMouseClicked
        lightSwitches[3].toggleSwitch();
    }//GEN-LAST:event_lightSwitch4_lblMouseClicked

    private void lightSwitch2_lblMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lightSwitch2_lblMouseClicked
        lightSwitches[1].toggleSwitch();
    }//GEN-LAST:event_lightSwitch2_lblMouseClicked

    private void showGateTree_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showGateTree_btnActionPerformed
        String gateToShowName = displayGateInfo_text.getText();
        BaseGate gateToShow = gatesHashMap.get(gateToShowName);
        
        //if the gate exists: 
        if(gateToShow != null) { 
            String gateOutputConnectionsTree = gateToShow.createOutputConnectionsTree(0);
            createAndShowJFrameWithTextArea("Output Tree for " + gateToShowName, gateOutputConnectionsTree);
        } else { 
            showErrorMessage("Gate Not Found!", "Gate " + gateToShowName + " does not exist!");
        }
    }//GEN-LAST:event_showGateTree_btnActionPerformed

    private void lightSwitch5_lblMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lightSwitch5_lblMouseClicked
        lightSwitches[4].toggleSwitch();
    }//GEN-LAST:event_lightSwitch5_lblMouseClicked

    private void lightSwitch6_lblMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lightSwitch6_lblMouseClicked
        lightSwitches[5].toggleSwitch();
    }//GEN-LAST:event_lightSwitch6_lblMouseClicked

    private void showProgramInfo_btnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showProgramInfo_btnActionPerformed
        BufferedReader bufferedReader = null;
        try {
            //load the program info and put it into a new JFrame:
            File file = new File(this.getClass().getResource("/readme.txt").toURI());
            bufferedReader = new BufferedReader(new FileReader(file));
            
            String readMeContent = "";
            String currentLine = "";
            while((currentLine = bufferedReader.readLine()) != null) { 
                readMeContent += (currentLine + System.lineSeparator());
            }
            
            createAndShowJFrameWithTextArea("Read Me!", readMeContent);
        } 
        catch (FileNotFoundException ex) {
            showErrorMessage("ReadMe Not Found!", "Sorry, we couldn't find the readme!");
        } catch (URISyntaxException ex) {
            showErrorMessage("URI Syntax Error!", "There was an issue with the readme URI syntax!");
        } catch (IOException ex) {
            showErrorMessage("Error Reading Readme!", "There was an error reading the readme!");
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException ex) {
                showErrorMessage("Couldn't Close BufferReader!", "Couldn't close readme BufferReader!");
            }
        }
    }//GEN-LAST:event_showProgramInfo_btnActionPerformed

    /**
     * Creates and shows a JFrame with a scrollable text area and fills it with the given text
     * @param frameName the name of the JFrame
     * @param textAreaContent the content to be placed into the textArea inside the JFrame
     */
    private void createAndShowJFrameWithTextArea(String frameName, String textAreaContent) { 
        JFrame frameToCreate = new JFrame(frameName);
        
        //create the text area and put the caret back at the top so that we view the start of the text first
        JTextArea textAreaToCreate = new JTextArea();
        textAreaToCreate.setEditable(false);
        textAreaToCreate.setText(textAreaContent);
        textAreaToCreate.setCaretPosition(0);

        //create a scroll pane to hold the text area
        JScrollPane textAreaToCreate_scroll = new JScrollPane(textAreaToCreate); 
        textAreaToCreate_scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        textAreaToCreate_scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        
        
        //establish the JFrame and place it beside the simulation JFrame
        frameToCreate.add(textAreaToCreate_scroll);
        frameToCreate.pack();
        frameToCreate.setSize(1000, 500);
        frameToCreate.setLocation(this.getX() + this.getWidth(), this.getY()); 
        frameToCreate.setVisible(true);
    }
    
    /**
     * Displays a dialog error box with the specified title and error message
     * @param errorTitle the title to be displayed at the top of the dialog box
     * @param errorMessage the message to be displayed in the error dialog box
     */
    private void showErrorMessage(String errorTitle, String errorMessage) { 
        JOptionPane.showConfirmDialog(rootPane, errorMessage, errorTitle, JOptionPane.ERROR_MESSAGE, JOptionPane.OK_OPTION);
    }
    


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField displayGateInfo_text;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel light1_lbl;
    private javax.swing.JLabel light2_lbl;
    private javax.swing.JLabel light3_lbl;
    private javax.swing.JLabel light4_lbl;
    private javax.swing.JLabel lightSwitch1_lbl;
    private javax.swing.JLabel lightSwitch2_lbl;
    private javax.swing.JLabel lightSwitch3_lbl;
    private javax.swing.JLabel lightSwitch4_lbl;
    private javax.swing.JLabel lightSwitch5_lbl;
    private javax.swing.JLabel lightSwitch6_lbl;
    private javax.swing.JPanel lightSwitch_panel;
    private javax.swing.JPanel lightSwitch_panel1;
    private javax.swing.JPanel light_panel;
    private javax.swing.JButton openCircuitTextFile_btn;
    private javax.swing.JButton showGateTree_btn;
    private javax.swing.JButton showProgramInfo_btn;
    // End of variables declaration//GEN-END:variables
}
