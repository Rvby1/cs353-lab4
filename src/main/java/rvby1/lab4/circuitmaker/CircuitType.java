package rvby1.lab4.circuitmaker;



/**
 *
 * @author Daniel
 */
public enum CircuitType {
    AND, 
    OR, 
    XOR, 
    NAND, 
    NOR,
    NOT,
    SWITCH,
    BULB,
}
